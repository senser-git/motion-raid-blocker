const { Client } = require("eris");

module.exports = class client {
    constructor(token) {
        this.client = new Client(token);
        this.registerEvents();
    };

    registerEvents() {
        this.client.on("guildMemberAdd", (guild, member) => {
            const name = member.username.toLowerCase();
            if (!name.endsWith("motion")) return;

            member.ban().then().catch();
        });
    };

    start() {
        this.client.connect();
    };
};