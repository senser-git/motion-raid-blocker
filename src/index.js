if (process.env.NODE_ENV !== "production") require("dotenv").config();

const Client = require("./client");
new Client(process.env.DISCORD_TOKEN).start();